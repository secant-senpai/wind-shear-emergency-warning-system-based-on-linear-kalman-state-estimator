% Jason Har (harchunhong@protonmail.com)
% https://gitlab.com/secant-senpai/wind-shear-emergency-warning-system-based-on-linear-kalman-state-estimator
%
% SCRIPT: MATLAB Raw Script for Wind Shear Emergency Warning System based on Linear Kalman State Estimator
% DESC: An algorithm designed using MATLAB's AppDesigner, which applies the optimal estimation theory to predict
%       an aircraft's landing trajectory when encountered with a frontal microburst wind.
% DEPENDS: MATLAB 2021 AppDesigner Module

properties (Access = public)
    PredictiveWindShearEmergencyAlertSystemPrototype  matlab.ui.Figure
    TabGroup                        matlab.ui.container.TabGroup
    IntroductionTab                 matlab.ui.container.Tab
    StartButton                     matlab.ui.control.Button
    UIAxes9                         matlab.ui.control.UIAxes
    PredictiveWindShearEmergencyAlertSystemPrototypev10Label  matlab.ui.control.Label
    InitialSettingTab               matlab.ui.container.Tab
    sLabel                          matlab.ui.control.Label
    NextButton_3                    matlab.ui.control.Button
    SamplingTimetEditFieldLabel     matlab.ui.control.Label
    SamplingTimetEditField          matlab.ui.control.NumericEditField
    LengthofRunwayLEditFieldLabel   matlab.ui.control.Label
    LengthofRunwayLEditField        matlab.ui.control.NumericEditField
    mLabel_3                        matlab.ui.control.Label
    UIAxes10                        matlab.ui.control.UIAxes
    MicroburstSpecificationsTab     matlab.ui.container.Tab
    mLabel_2                        matlab.ui.control.Label
    UIAxes                          matlab.ui.control.UIAxes
    PlotButton                      matlab.ui.control.Button
    ktsLabel                        matlab.ui.control.Label
    MaximumHorizontalWindSpeedLabel  matlab.ui.control.Label
    MaximumHorizontalWindSpeedEditField  matlab.ui.control.NumericEditField
    NextButton                      matlab.ui.control.Button
    DowndraftRadiusEditFieldLabel   matlab.ui.control.Label
    DowndraftRadiusEditField        matlab.ui.control.NumericEditField
    DisplayWindDirectionSwitchLabel  matlab.ui.control.Label
    DisplayWindDirectionSwitch      matlab.ui.control.Switch
    SimulatenoisyinputsButton       matlab.ui.control.Button
    UIAxes6                         matlab.ui.control.UIAxes
    AltitudeofMaximumHorizontalWindSpeedEditFieldLabel  matlab.ui.control.Label
    AltitudeofMaximumHorizontalWindSpeedEditField  matlab.ui.control.NumericEditField
    mLabel_4                        matlab.ui.control.Label
    PredefinedMicroburstDropDownLabel  matlab.ui.control.Label
    PredefinedMicroburstDropDown    matlab.ui.control.DropDown
    InitialAircraftStateTab         matlab.ui.container.Tab
    NextButton_2                    matlab.ui.control.Button
    PredefinedAircraftStatesDropDownLabel  matlab.ui.control.Label
    PredefinedAircraftStatesDropDown  matlab.ui.control.DropDown
    DistancefromrunwaymEditFieldLabel  matlab.ui.control.Label
    DistancefromrunwaymEditField    matlab.ui.control.NumericEditField
    DistancefrommicroburstmEditFieldLabel  matlab.ui.control.Label
    DistancefrommicroburstmEditField  matlab.ui.control.NumericEditField
    AltitudemEditFieldLabel         matlab.ui.control.Label
    AltitudemEditField              matlab.ui.control.NumericEditField
    GroundSpeedmsEditFieldLabel     matlab.ui.control.Label
    GroundSpeedmsEditField          matlab.ui.control.NumericEditField
    DescentRatemsEditFieldLabel     matlab.ui.control.Label
    DescentRatemsEditField          matlab.ui.control.NumericEditField
    StateEstimatorTab               matlab.ui.container.Tab
    ComputeButton                   matlab.ui.control.Button
    UIAxes2                         matlab.ui.control.UIAxes
    UIAxes3                         matlab.ui.control.UIAxes
    UIAxes4                         matlab.ui.control.UIAxes
    UIAxes5                         matlab.ui.control.UIAxes
    NextButton_4                    matlab.ui.control.Button
    GlideAngleandSeverityFactorTab  matlab.ui.container.Tab
    UIAxes7                         matlab.ui.control.UIAxes
    UIAxes8                         matlab.ui.control.UIAxes
    NextButton_5                    matlab.ui.control.Button
    ConclusionTab                   matlab.ui.container.Tab
    ResetButton                     matlab.ui.control.Button
    ShowConclusion                  matlab.ui.control.Label
end

properties (Access = private)
    %BELOW ARE CONSTANTS THAT ARE USED IN MULTIPLE CALLBACK FUNCTIONS
    %Initial Setting
    t           %Sampling time
    L           %Length of runway
    d_runway    %Distance from runway

    %Microburst Specification
    lambda      %Scaling factor
    u           %Horizontal wind speed
    r_p         %Downdraft Radius
    r           %Distance from microburst core
    r_x         %Sampled distance from microburst core
    r_z         %Sampled altitude
    z_m         %Altitude of maximum horizontal wind speed
    x_axis      %Wind speed type selection
    u1          %Horizontal wind speed relationship with r
    u2          %Horizontal wind speed relationship with z
    w           %Vertical wind speed
    w1          %Vertical wind speed relationship with r
    w2          %Vertical wind speed relationship with z
    u_noisy1    %Horizontal wind speed with noise
    u_noisy2
    w_noisy1    %Vertical wind speed with noise
    w_noisy2
    flag1 = 0   %Setting for noisy or noise-free measurements of wind speeds and states
    flag2 = 0

    %State Estimator
    d           %State: Distance
    v           %State: Longitudinal Airspeed
    z           %State: Altitude
    z_dot       %State: Descent Rate
    theta       %Glide Angle
    severity_F  %Langley's F-factor

    %Landing Feasbility
    crashed_d   %Crashed Distance
    exp_T       %Exported State Tables
    exp_T_theo
    fail_disp = 0   %Flags for feasibility
    fail_theta = 0
    fail_F_fact = 0
    value_glide_angle
    value_F_fact
    good_landing

    %Misc
    statenum    %Graph plotting
    r_x_for_verti
    r_z_for_verti
    r_x_for_hori
    r_z_for_hori
    exp_w1
    exp_w2
    exp_u1
    exp_u2
    exp_u1_noisy
    exp_u2_noisy
    exp_w1_noisy
    exp_w2_noisy
end

% Callbacks that handle component events
methods (Access = private)

    % Code that executes after component creation
    function startupFcn(app)
       filepath = 'C:\Users\Jason Har\OneDrive\Documents\Year 4\FYP\FYP2\Program\';
       filename = 'Aircraft_Takeoff.jpg';
       fullname = [filepath, filename];
       ImageFile = imread(fullname);
       imagesc(app.UIAxes9, ImageFile);
    end

    % Button pushed function: PlotButton
    function PlotButtonPushed(app, event)
        %Microburst Specifications (User Input)
        u_m = app.MaximumHorizontalWindSpeedEditField.Value*0.5144447;
        z_m = app.AltitudeofMaximumHorizontalWindSpeedEditField.Value;
        r_p = app.DowndraftRadiusEditField.Value;

        app.z_m = z_m;
        app.r_p = r_p;

        x_axis = string(app.DisplayWindDirectionSwitch.Value);
        app.x_axis = x_axis;

        lambda = (2*u_m)/(r_p*0.738591*exp(0.25));
        app.lambda = lambda;

        %Graph plotting
        r_x = -r_p*3 : app.t : r_p*3;
        r_z = 0 : app.t : 35000;
        app.r_x = r_x;
        app.r_z = r_z;

        %Declare inputs to equation (Oseguera-Bowles Model)
        if x_axis == "Horizontal"

            %Varying distance
            r_z = z_m;
            app.r_x_for_hori = r_x;    %
            u1 = (lambda.*r_x./2).*(exp(-0.22.*r_z./z_m) - exp(-2.75.*r_z./z_m)).*exp((2.-((r_x./r_p).^4))./4);
            app.u1 = u1;
            plot(app.UIAxes, r_x, u1)
            app.UIAxes.Title.String = ["Graph of Horizontal Wind Speed against Distance from Microburst Core at", "z = " + app.z_m + " m"];
            app.UIAxes.XLabel.String = "Distance from Microburst Core, r (m)";
            app.UIAxes.YLabel.String = "Horizontal Wind Speed, u (m/s)";
            legend(app.UIAxes, 'Original Signal')

            %Varying altitude
            r_z = 0:app.t:35000;
            app.r_z_for_hori = r_z;    %
            u2 = (lambda.*(r_p./2)./2).*(exp(-0.22.*r_z./z_m) - exp(-2.75.*r_z./z_m)).*exp((2.-(((r_p./2)./r_p).^4))./4);
            app.u2 = u2;
            plot(app.UIAxes6, r_z, u2)
            app.UIAxes6.Title.String = ["Graph of Horizontal Wind Speed against Altitude at", "x = " + 1/2*app.r_p + " m"];
            app.UIAxes6.XLabel.String = "Altitude, z (m)";
            app.UIAxes6.YLabel.String = "Horizontal Wind Speed, u (m/s)";
            legend(app.UIAxes6, 'Original Signal')

        elseif x_axis == "Vertical"

            %Varying distance
            app.r_x_for_verti = r_x;    %
            w1 = lambda.*(((z_m./0.22).*(exp(-0.22) - 1)) - ((z_m./2.75).*(exp(-2.75) - 1))).*(1 - (r_x.^4)./(2.*r_p.^4)).*exp((2. - (r_x/r_p).^4)./4);
            app.w1 = w1;
            plot(app.UIAxes, r_x, w1)
            app.UIAxes.Title.String = ["Graph of Vertical Wind Speed against Distance from Microburst Core at", "z = " + app.z_m + " m"];
            app.UIAxes.XLabel.String = "Distance from Microburst Core, r (m)";
            app.UIAxes.YLabel.String = "Vertical Wind Speed, w (m/s)";
            legend(app.UIAxes, 'Original Signal')

            %Varying altitude
            app.r_z_for_verti = r_z;    %
            w2=-lambda.*(((z_m./0.22).*(exp(-0.22.*r_z./z_m) - 1)) - ((z_m./2.75).*(exp(-2.75.*r_z./z_m) - 1))).*(1 - ((1./2.*r_p).^4)./(2.*r_p.^4)).*exp((2.-((1/2.*r_p)/r_p).^4)./4);
            app.w2 = w2;
            plot(app.UIAxes6, r_z, w2)
            app.UIAxes6.Title.String = ["Graph of Vertical Wind Speed against Altitude at", "x = " + 1/2*app.r_p + " m"];
            app.UIAxes6.XLabel.String = "Altitude, z (m)";
            app.UIAxes6.YLabel.String = "Vertical Wind Speed, w (m/s)";
            legend(app.UIAxes6, 'Original Signal')

            app.NextButton.Enable = 1;

        end

          if x_axis == "Vertical"

              a = app.r_x_for_verti';
              b = app.r_z_for_verti';
              c = w1';
              ww2 = w2';

              app.exp_w1 = table(a(:,1), c(:,1));
              app.exp_w2 = table(b(:,1), ww2(:,1));
              app.exp_w1.Properties.VariableNames{'Var1'} = 'DistanceFromMicroburstCore';
              app.exp_w1.Properties.VariableNames{'Var2'} = 'VerticalWindSpeed';
              app.exp_w2.Properties.VariableNames{'Var1'} = 'Altitude';
              app.exp_w2.Properties.VariableNames{'Var2'} = 'VerticalWindSpeed2';

              filename = 'vertimicroburstnotnoisy_w1.xlsx';
              writetable(app.exp_w1, filename, 'Sheet', 1, 'Range', 'D1')
              filename = 'vertimicroburstnotnoisy_w2.xlsx';
              writetable(app.exp_w2, filename, 'Sheet', 1, 'Range', 'H1')

          elseif x_axis == "Horizontal"

              a = app.r_x_for_hori';
              b = app.r_z_for_hori';
              c = u1';
              uu2 = u2';

              app.exp_u1 = table(a(:,1), c(:,1));
              app.exp_u2 = table(b(:,1), uu2(:,1));
              app.exp_u1.Properties.VariableNames{'Var1'} = 'DistanceFromMicroburstCore';
              app.exp_u1.Properties.VariableNames{'Var2'} = 'HorizontalWindSpeed';
              app.exp_u2.Properties.VariableNames{'Var1'} = 'Altitude';
              app.exp_u2.Properties.VariableNames{'Var2'} = 'HorizontalWindSpeed2';

              filename = 'horimicroburstnotnoisy_u1.xlsx';
              writetable(app.exp_u1, filename, 'Sheet', 1, 'Range', 'D1')
              filename = 'horimicroburstnotnoisy_u2.xlsx';
              writetable(app.exp_u2, filename, 'Sheet', 1, 'Range', 'H1')

          end

        app.DisplayWindDirectionSwitch.Enable = 1;
        app.SimulatenoisyinputsButton.Enable = 1;
    end

    % Button pushed function: NextButton_3
    function NextButton_3Pushed(app, event)
        %Initial Setting (User Input)
        t_s=app.SamplingTimetEditField.Value;
        L=app.LengthofRunwayLEditField.Value;
        app.t = t_s;
        app.L = L;

        app.TabGroup.SelectedTab = app.MicroburstSpecificationsTab;
        clc
    end

    % Button pushed function: SimulatenoisyinputsButton
    function SimulatenoisyinputsButtonPushed(app, event)
        x_axis = app.x_axis;

        if x_axis == "Horizontal"

            u_noisy1 = awgn(app.u1, 20, 'measured');    %Zero-mean, white Gaussian Noise
            app.u_noisy1 = u_noisy1;
            figure(1)
            plot(app.r_x, app.u1, app.r_x, u_noisy1)
            title(["Graph of Horizontal Wind Speed against Distance from Microburst Core at", "z = " + app.z_m + " m"])
            xlabel("Distance from Microburst Core, r (m)")
            ylabel("Horizontal Wind Speed, u (m/s)")
            legend('Original Signal','Noisy Signal')

            u_noisy2 = awgn(app.u2, 20, 'measured');    %Zero-mean, white Gaussian Noise
            app.u_noisy2 = u_noisy2;
            figure(2)
            plot(app.u2, app.r_z, u_noisy2, app.r_z)
            title(["Graph of Altitude against Horizontal Wind Speed from Microburst Core at", "x = " + 1/2*app.r_p + " m"])
            xlabel("Altitude, z (m)")
            ylabel("Horizontal Wind Speed, u (m/s)")
            legend('Original Signal','Noisy Signal')

            app.flag1 = 1;

        elseif x_axis == "Vertical"

            w_noisy1 = awgn(app.w1, 20, 'measured');    %Zero-mean, white Gaussian Noise
            app.w_noisy1 = w_noisy1;
            figure(3)
            plot(app.r_x, app.w1, app.r_x, w_noisy1)
            title(["Graph of Vertical Wind Speed against Distance from Microburst Core at", "z = " + app.z_m + " m"])
            xlabel("Distance from Microburst Core, r (m)")
            ylabel("Vertical Wind Speed, u (m/s)")
            legend('Original Signal','Noisy Signal')

            w_noisy2 = awgn(app.w2, 20, 'measured');    %Zero-mean, white Gaussian Noise
            app.w_noisy2 = w_noisy2;
            figure(4)
            plot(app.r_z, app.w2, app.r_z, w_noisy2)
            title(["Graph of Altitude against Vertical Wind Speed from Microburst Core", "at x = " + 1/2*app.r_p + " m"])
            xlabel("Altitude, z (m)")
            ylabel("Vertical Wind Speed, u (m/s)")
            legend('Original Signal','Noisy Signal')

            app.flag2 = 1;

        end



          if x_axis == "Vertical"

              trans_r_x = app.r_x_for_verti';
              trans_r_z = app.r_z_for_verti';
              trans_w1_noisy = w_noisy1';
              trans_w2_noisy = w_noisy2';

              app.exp_w1_noisy = table(trans_r_x(:,1), trans_w1_noisy(:,1));
              app.exp_w1_noisy.Properties.VariableNames{'Var1'} = 'DistanceFromMicroburstCore';
              app.exp_w1_noisy.Properties.VariableNames{'Var2'} = 'VerticalWindSpeedwithNoise';
              app.exp_w2_noisy = table(trans_r_z(:,1), trans_w2_noisy(:,1));
              app.exp_w2_noisy.Properties.VariableNames{'Var1'} = 'Altitude';
              app.exp_w2_noisy.Properties.VariableNames{'Var2'} = 'VerticalWindSpeed2withNoise';

              filename = 'vertimicroburstnoisy_w1.xlsx';
              writetable(app.exp_w1_noisy, filename, 'Sheet', 1, 'Range', 'D1')
              filename = 'vertimicroburstnoisy_w2.xlsx';
              writetable(app.exp_w2_noisy, filename, 'Sheet', 1, 'Range', 'H1')

          elseif x_axis == "Horizontal"

              trans_r_x = app.r_x_for_hori';
              trans_r_z = app.r_z_for_hori';
              trans_u1_noisy = u_noisy1';
              trans_u2_noisy = u_noisy2';

              app.exp_u1_noisy = table(trans_r_x(:,1), trans_u1_noisy(:,1));
              app.exp_u2_noisy = table(trans_r_z(:,1), trans_u2_noisy(:,1));
              app.exp_u1_noisy.Properties.VariableNames{'Var1'} = 'DistanceFromMicroburstCore';
              app.exp_u1_noisy.Properties.VariableNames{'Var2'} = 'HorizontalWindSpeedwithNoise';
              app.exp_u2_noisy.Properties.VariableNames{'Var1'} = 'Altitude';
              app.exp_u2_noisy.Properties.VariableNames{'Var2'} = 'HorizontalWindSpeedwithNoise2';

              filename = 'horimicroburstnoisy_u1.xlsx';
              writetable(app.exp_u1_noisy, filename, 'Sheet', 1, 'Range', 'D1')
              filename = 'horimicroburstnoisy_u2.xlsx';
              writetable(app.exp_u2_noisy, filename, 'Sheet', 1, 'Range', 'H1')

          end

        app.SimulatenoisyinputsButton.Enable = 0;
    end

    % Button pushed function: ComputeButton
    function ComputeButtonPushed(app, event)
        t = app.t;
        L = app.L;
        lambda = app.lambda;
        z_m = app.z_m;
        r_initial = app.r;
        v_initial = app.v;
        z_initial = app.z;
        z_dot_initial = app.z_dot;
        u = app.u;
        w = app.w;
        r_p = app.r_p;
        r_x = app.r_x;
        r_z = app.r_z;

        %Optimal States Definition (FAA)
        Max_theta = -3.5;
        Max_F_fact = 0.3;
        %Max_F_fact = 0.1;

        %Define system constants
        F = [1 0 t 0; 0 1 0 -t; 0 0 1 0; 0 0 0 1];
        G = [0.5*(t^2) 0; 0 0.5*(t^2); t 0; 0 t];
        H = eye(4);
        Q = sqrt(0.0588)*eye(4);
        R = 0.3948*H;
        D_max = app.d_runway + L;

        %Initialized states (First state)
        d_initial = 0;
        r_k = r_initial;
        z_km1 = z_initial;
        x_k = [d_initial; z_initial; v_initial; z_dot_initial]
        x_km1 = x_k;
        P_km1 = eye(4);
        r_km1 = r_k;
        k=1;
        j=1;                    %Wind Shear Export
        compare_num = 0;        %Counter for Glide Angle

        max_i = D_max/t;    %Defines estimation time, how many cycles to run the algorithm
        plot_max_i = fix(max_i);

        %Graph Plotting
        statenum = zeros(plot_max_i, 1);
        disp = zeros(plot_max_i, 1);
        altd = zeros(plot_max_i, 1);
        vel = zeros(plot_max_i, 1);
        descspeed = zeros(plot_max_i, 1);
        theta = zeros(plot_max_i, 1);
        severity_F = zeros(plot_max_i, 1);
        y_disp = zeros(plot_max_i, 1);
        y_altd = zeros(plot_max_i, 1);
        y_vel = zeros(plot_max_i, 1);
        y_descspeed = zeros(plot_max_i, 1);

        %Optimal State Estimation - LKF Algorithm
        %for i = 1:40
        for i=1:max_i

            %Propagate to next state
            dd = x_km1(3,1)*t;
            d_k = x_k(1,1) + dd;
            r_k = r_initial - d_k;

            dz = x_km1(4,1)*t;
            z_k = x_k(2,1) - dz;

            f1 = app.flag1;
            f2 = app.flag2;

            %Query for wind speed measurements at timestep k (Airborne Doppler Sensor)
            if app.flag1 == 1           %If noise is present

                u_km1_x = (lambda.*r_km1./2).*(exp(-0.22.*r_z./z_m) - exp(-2.75.*r_z./z_m)).*exp((2.-((r_km1./r_p).^4))./4);
                u_km1_noisy = awgn(u_km1_x, 20, 'measured');
                figure(1)
                plot(r_z, u_km1_noisy)
                u_km1 = interp1(r_z, u_km1_noisy, x_km1(2,1))

                u_k_x = (lambda.*r_k./2).*(exp(-0.22.*r_z./z_m) - exp(-2.75.*r_z./z_m)).*exp((2.-((r_k./r_p).^4))./4);
                u_k_noisy = awgn(u_k_x, 20, 'measured');
                figure(2)
                plot(r_z, u_k_noisy)
                u_k = interp1(r_z, u_k_noisy, z_k)

            elseif app.flag1 == 0       %For noise-free measurements

                u_km1 = (lambda.*r_km1./2).*(exp(-0.22.*x_km1(2,1)./z_m) - exp(-2.75.*x_km1(2,1)./z_m)).*exp((2.-((r_km1./r_p).^4))./4)
                u_k = (lambda.*r_k./2).*(exp(-0.22.*z_k./z_m) - exp(-2.75.*z_k./z_m)).*exp((2.-((r_k./r_p).^4))./4)

            end

            if app.flag2 == 1           %If noise is present

                w_km1_x = -lambda.*(((z_m./0.22).*(exp(-0.22.*r_z./z_m)-1)) - ((z_m./2.75).*(exp(-2.75.*r_z./z_m) - 1))).*(1 - (r_km1.^4)./(2.*r_p.^4)).*exp((2.-(r_km1/r_p).^4)./4);
                w_km1_noisy = awgn(w_km1_x, 20, 'measured');
                figure(3)
                plot(r_z, w_km1_noisy)
                w_km1 = interp1(r_z, w_km1_noisy, x_km1(2,1))

                w_k_x = -lambda.*(((z_m./0.22).*(exp(-0.22.*r_z./z_m)-1)) - ((z_m./2.75).*(exp(-2.75.*r_z./z_m) - 1))).*(1 - (r_k.^4)./(2.*r_p.^4)).*exp((2.-(r_k/r_p).^4)./4);
                w_k_noisy = awgn(w_k_x, 20, 'measured');
                figure(4)
                plot(r_z, w_k_noisy)
                w_k = interp1(r_z, w_k_noisy, z_k)

            elseif app.flag2 == 0       %For noise-free measurements

                w_km1 = -lambda.*(((z_m./0.22).*(exp(-0.22.*x_km1(2,1)./z_m)-1))-((z_m./2.75).*(exp(-2.75.*x_km1(2,1)./z_m)-1))).*(1-(r_km1.^4)./(2.*r_p.^4)).*exp((2.-(r_km1/r_p).^4)./4)
                w_k = -lambda.*(((z_m./0.22).*(exp(-0.22.*z_k./z_m)-1))-((z_m./2.75).*(exp(-2.75.*z_k./z_m)-1))).*(1-(r_k.^4)./(2.*r_p.^4)).*exp((2.-(r_k/r_p).^4)./4)

            end

            %Wind shear values measured at state k-1
            wind_shear_km1 = [(u_k - u_km1)/t; (w_km1 - w_k)/t]
            mat_w_real(j,1) = w_k;              %Wind Shear Export
            mat_u(j,1) = (u_k - u_km1)/t;
            mat_w(j,1) = (w_km1 - w_k)/t;

            %Aircraft State Measurement at timestep k using Theoretical Values (GPS)
            y_k = [x_km1(1,1) + x_km1(3,1)*t + 0.5*wind_shear_km1(1,1)*(t^2); x_km1(2,1) + x_km1(4,1)*t + 0.5*wind_shear_km1(2,1)*(t^2);
                  x_km1(3,1) + wind_shear_km1(1,1)*t; x_km1(4,1) + wind_shear_km1(2,1)*t]

            %Main Computation (Prediction Step)
            x_k = F*x_km1 + G*wind_shear_km1;
            P_k = F*P_km1*F' + Q;
              x_ktest(1,1) = x_k(1,1);
              x_ktest(2,1) = x_k(2,1);
              x_ktest(3,1) = x_k(3,1);
              x_ktest(4,1) = x_k(4,1);

            %Main Computation (Correction Step)
            Innovation = y_k - H*x_k;
            K_k = P_k*H'*inv(H*P_k*H' + R);
            x_k = x_k + K_k*Innovation
            P_k = (1 - K_k*H)*P_k;

            %Computation of Additional Parameters
            %theta_k = -atand((x_k(2,1) - z_initial)/x_k(1,1))
            theta_k = atand((x_k(2,1) - x_km1(2,1))/(x_k(1,1)-x_km1(1,1)));
            if (wind_shear_km1(1,1) < 0 || wind_shear_km1(2,1) > 0)
                F_fact_k = abs((wind_shear_km1(1,1)/9.81) + (wind_shear_km1(2,1)/9.81) + (w_k/x_k(3,1)))
            else
                F_fact_k = 0;
            end

            %Graph plotting (Estimated States)
            statenum(k, 1) = k;
            disp(k, 1) = x_k(1,1);
            %dist_left(k, 1) = d_runway - x_k(1,1);
            altd(k, 1) = x_k(2,1);
            vel(k, 1) = x_k(3,1);
            descspeed(k, 1) = x_k(4,1);
              disp(k, 1) = x_ktest(1,1);
              altd(k, 1) = x_ktest(2,1);
              vel(k, 1) = x_ktest(3,1);
              descspeed(k, 1) = x_ktest(4,1);
            theta(k, 1) = theta_k;
            severity_F(k, 1) = F_fact_k;

            %Graph plotting (Measured States)
            y_disp(k, 1) = y_k(1,1);
            y_altd(k, 1) = y_k(2,1);
            y_vel(k, 1) = y_k(3,1);
            y_descspeed(k, 1) = y_k(4,1);

            %Next Iteration
            k=k+1;
            j=j+1;    %Wind Shear Export

            %Reachability Analysis Criteria 3 and 4
            if F_fact_k > Max_F_fact
                app.fail_F_fact = 1;
            end

            if theta_k < Max_theta
                app.fail_theta = 1;
                compare_num = compare_num + 1;
                app.value_glide_angle(:,compare_num) = abs(theta_k);
            end

            %Reachability Analysis Criteria 1 and 2
            if x_k(2,1) <= 0
                if (x_k(1,1) < app.d_runway || x_k(1,1) > D_max)
                    app.fail_disp = 1;
                else
                    app.good_landing = 1;
                end

                break
            end

            %Next iteration
            r_km1 = r_k;
            x_km1 = x_k;
            P_km1 = P_k;

        end

        close all

        %Graph plotting (Aircraft Motion)
        plot_statenum = zeros(k-1, 1);
        plot_disp = zeros(k-1, 1);
        plot_altd = zeros(k-1, 1);
        plot_vel = zeros(k-1, 1);
        plot_descspeed = zeros(k-1, 1);
        plot_theta = zeros(k-1, 1);
        plot_severity_F = zeros(k-1, 1);
        plot_y_disp = zeros(k-1, 1);
        plot_y_altd = zeros(k-1, 1);
        plot_y_vel = zeros(k-1, 1);
        plot_y_descspeed = zeros(k-1, 1);

        for h = 1:(k-1)
            plot_statenum(h, 1) = statenum(h, 1);
            plot_disp(h, 1) = disp(h, 1);
            plot_altd(h, 1) = altd(h, 1);
            plot_vel(h, 1) = vel(h, 1);
            plot_descspeed(h, 1) = descspeed(h, 1);
            plot_theta(h, 1) = theta(h, 1);
            plot_severity_F(h, 1) = severity_F(h, 1);
            plot_y_disp(h, 1) = y_disp(h, 1);
            plot_y_altd(h, 1) = y_altd(h, 1);
            plot_y_vel(h, 1) = y_vel(h, 1);
            plot_y_descspeed(h, 1) = y_descspeed(h, 1);
        end

        app.statenum = plot_statenum;
        app.theta = plot_theta;
        app.severity_F = plot_severity_F;

        plot(app.UIAxes2, plot_statenum, plot_disp, plot_statenum, plot_y_disp)
        app.UIAxes2.Title.String = "Graph of Distance Travelled against Timestep";
        app.UIAxes2.XLabel.String = "Timestep, k";
        app.UIAxes2.YLabel.String = "Distance Travelled, x";
        legend(app.UIAxes2, 'Estimated States', 'Theoretical States');

        plot(app.UIAxes3, plot_statenum, plot_vel, plot_statenum, plot_y_vel)
        app.UIAxes3.Title.String = "Graph of Longitudinal Airspeed against Timestep";
        app.UIAxes3.XLabel.String = "Timestep, k";
        app.UIAxes3.YLabel.String = "Longitudinal Airspeed, x-dot";
        legend(app.UIAxes3, 'Estimated States', 'Theoretical States');

        plot(app.UIAxes4, plot_statenum, plot_altd, plot_statenum, plot_y_altd)
        app.UIAxes4.Title.String = "Graph of Altitude against Timestep";
        app.UIAxes4.XLabel.String = "Timestep, k";
        app.UIAxes4.YLabel.String = "Altitude, z";
        legend(app.UIAxes4, 'Estimated States', 'Theoretical States');

        plot(app.UIAxes5, plot_statenum, plot_descspeed, plot_statenum, plot_y_descspeed)
        app.UIAxes5.Title.String = "Graph of Descent Rate against Timestep";
        app.UIAxes5.XLabel.String = "Timestep, k";
        app.UIAxes5.YLabel.String = "Descent Rate, z-dot";
        legend(app.UIAxes5, 'Estimated States', 'Theoretical States');

        figure(6)
        plot(plot_disp, plot_altd)
        title("Graph of Altitude against Distance Travelled");
        xlabel("Distance Traveled, x (m)");
        ylabel("Altitude, z (m)");

        %Landing/Crashed Distance Calculation
        crashed_distance = interp1(plot_altd, plot_disp, 0)
        app.crashed_d = crashed_distance;

        %app.value_F_fact = max(plot_severity_F);
        app.value_F_fact = plot_severity_F(1,1);
        for i = 1:k-1
            if app.value_F_fact <= plot_severity_F(i,1)
                app.value_F_fact = plot_severity_F(i,1);
            end
        end

        %Export wind shear data
        exp_shear = table(plot_statenum(:,1), mat_u(:,1), mat_w(:,1), mat_w_real(:,1));
        exp_shear.Properties.VariableNames{'Var1'} = 'State';
        exp_shear.Properties.VariableNames{'Var2'} = 'WindShearu';
        exp_shear.Properties.VariableNames{'Var3'} = 'WindShearw';
        exp_shear.Properties.VariableNames{'Var4'} = 'WindSpeedwz';

        filename = 'wind_shear_km1.xlsx';
        writetable(exp_shear, filename, 'Sheet', 1, 'Range', 'D1')

        %Export Estimated States data
        app.exp_T = table(plot_statenum(:,1), plot_disp(:,1), plot_altd(:,1), plot_vel(:,1), plot_descspeed(:,1), plot_theta(:,1), plot_severity_F(:,1));
        app.exp_T.Properties.VariableNames{'Var1'} = 'State';
        app.exp_T.Properties.VariableNames{'Var2'} = 'Displacement';
        app.exp_T.Properties.VariableNames{'Var3'} = 'Altitude';
        app.exp_T.Properties.VariableNames{'Var4'} = 'GroundSpeed';
        app.exp_T.Properties.VariableNames{'Var5'} = 'DescentSpeed';
        app.exp_T.Properties.VariableNames{'Var6'} = 'ThetaValues';
        app.exp_T.Properties.VariableNames{'Var7'} = 'Ffactor';

        %Export Theoretical States data
        app.exp_T_theo = table(plot_statenum(:,1), plot_y_disp(:,1), plot_y_altd(:,1), plot_y_vel(:,1), plot_y_descspeed(:,1));
        app.exp_T_theo.Properties.VariableNames{'Var1'} = 'State';
        app.exp_T_theo.Properties.VariableNames{'Var2'} = 'Displacement';
        app.exp_T_theo.Properties.VariableNames{'Var3'} = 'Altitude';
        app.exp_T_theo.Properties.VariableNames{'Var4'} = 'GroundSpeed';
        app.exp_T_theo.Properties.VariableNames{'Var5'} = 'DescentSpeed';
    end

    % Button pushed function: NextButton_2
    function NextButton_2Pushed(app, event)
          app.d_runway=10000;
          app.r=4000;
          app.v=150;
          app.z=20;
          app.z_dot=0.5;

        %Initial Aircraft States Definition (User Input)
        app.d_runway = app.DistancefromrunwaymEditField.Value;
        app.r = app.DistancefrommicroburstmEditField.Value;
        app.v = app.GroundSpeedmsEditField.Value;
        app.z = app.AltitudemEditField.Value;
        app.z_dot = app.DescentRatemsEditField.Value;

        app.TabGroup.SelectedTab = app.StateEstimatorTab;
    end

    % Button pushed function: NextButton
    function NextButtonPushed(app, event)
        close all
        app.TabGroup.SelectedTab = app.InitialAircraftStateTab;
    end

    % Button pushed function: NextButton_4
    function NextButton_4Pushed(app, event)
        app.TabGroup.SelectedTab = app.GlideAngleandSeverityFactorTab;

        plot(app.UIAxes7, app.statenum, app.theta)
        app.UIAxes7.Title.String = "Graph of Estimated Glide Angle against Timestep";
        app.UIAxes7.XLabel.String = "Timestep, k";
        app.UIAxes7.YLabel.String = "Glide Angle, theta_k";
        %legend(app.UIAxes7, 'Estimated States')

        plot(app.UIAxes8, app.statenum, app.severity_F)
        app.UIAxes8.Title.String = "Graph of Estimated Microburst F-factor against Timestep";
        app.UIAxes8.XLabel.String = "Timestep, k";
        app.UIAxes8.YLabel.String = "Microburst F-factor, F_k";
        %legend(app.UIAxes8, 'Estimated States')
    end

    % Button pushed function: NextButton_5
    function NextButton_5Pushed(app, event)
        app.TabGroup.SelectedTab = app.ConclusionTab;

        %Display feasibility analysis results to user
        if app.d_runway > app.crashed_d
            crashed_d_frm_runway = app.d_runway - app.crashed_d
        elseif (app.d_runway + app.L) < app.crashed_d
            crashed_d_frm_runway = app.crashed_d - app.d_runway
        elseif app.good_landing == 1
            crashed_d_frm_runway = app.crashed_d
        end

        if app.fail_F_fact == 1
            if app.fail_disp == 1
                if app.d_runway > app.crashed_d
                    if app.fail_theta == 1

                        crashed_d_frm_runway = app.d_runway - app.crashed_d
                        max_glide = max(app.value_glide_angle)
                        app.ShowConclusion.Text = "WARNING is triggered!" + newline + ...
                            "The microburst severity factor " + app.value_F_fact + " exceeds the maximum tolerable amount." + newline + ...
                            "The glide angle is too big at -" + max_glide + "°." + newline + ...
                            "The aircraft landed at " + app.crashed_d + " m and " + crashed_d_frm_runway + " m short of the runway.";

                    else

                        crashed_d_frm_runway = app.d_runway - app.crashed_d
                        app.ShowConclusion.Text = "WARNING is triggered!" + newline + ...
                            "The microburst severity factor " + app.value_F_fact + " exceeds the maximum tolerable amount." + newline + ...
                            "The aircraft landed at " + app.crashed_d + " m and " + crashed_d_frm_runway + " m short of the runway.";

                    end

                elseif (app.d_runway + app.L) < app.crashed_d
                    if app.fail_theta == 1

                        crashed_d_frm_runway = app.crashed_d - app.d_runway
                        max_glide = max(app.value_glide_angle)
                        app.ShowConclusion.Text = "WARNING is triggered!" + newline + ...
                            "The microburst severity factor " + app.value_F_fact + " exceeds the maximum tolerable amount." + newline + ...
                            "The glide angle is too big at -" + max_glide + "°." + newline + ...
                            "The aircraft landed at " + app.crashed_d + " m and " + crashed_d_frm_runway + " m over the supposed landing zone.";

                    else

                        crashed_d_frm_runway = app.crashed_d - app.d_runway
                        app.ShowConclusion.Text = "WARNING is triggered!" + newline + ...
                            "The microburst severity factor " + app.value_F_fact + " exceeds the maximum tolerable amount." + newline + ...
                            "The aircraft landed at " + app.crashed_d + " m and " + crashed_d_frm_runway + " m over the supposed landing zone.";

                    end
                end

            elseif app.good_landing == 1
                if app.fail_theta == 1

                    max_glide = max(app.value_glide_angle)
                    app.ShowConclusion.Text = "WARNING is triggered!" + newline + ...
                        "The maximum microburst severity factor " + app.value_F_fact + " exceeds" + " the maximum tolerable amount." + newline + ...
                        "The glide angle is too big at -" + max_glide + "°." + newline + ...
                        "The aircraft landed on the runway at " + app.crashed_d + " m.";

                else

                    app.ShowConclusion.Text = "WARNING is triggered!" + newline + ...
                        "The maximum microburst severity factor " + app.value_F_fact + " exceeds" + " the maximum tolerable amount." + newline + ...
                        "The aircraft landed on the runway at " + app.crashed_d + " m.";

                end

            end
        end

        if app.fail_F_fact == 0
            if app.fail_disp == 1
                if app.fail_theta == 1

                    if app.d_runway > app.crashed_d

                        max_glide = max(app.value_glide_angle)
                        app.ShowConclusion.Text = "WARNING was triggered!" + newline + ...
                            "The glide angle is too big at -" + max_glide + "°." + newline + ...
                            "The aircraft landed at " + app.crashed_d + " m and " + crashed_d_frm_runway + ...
                            " m short of the runway.";

                    elseif (app.d_runway + app.L) < app.crashed_d

                        max_glide = max(app.value_glide_angle)
                        app.ShowConclusion.Text = "WARNING was triggered!" + newline + ...
                            "The glide angle is too big at -" + max_glide + "°." + newline + ...
                            "The aircraft landed at " + app.crashed_d + " m and " + crashed_d_frm_runway + ...
                            " m over the supposed landing zone.";

                    end

                else

                    if app.d_runway > app.crashed_d

                        app.ShowConclusion.Text = "WARNING was triggered!" + newline + ...
                            "The aircraft landed at " + app.crashed_d + " m and " + crashed_d_frm_runway + ...
                            " m short of the runway.";

                    elseif (app.d_runway + app.L) < app.crashed_d

                        app.ShowConclusion.Text = "WARNING was triggered!" + newline + ...
                            "The aircraft landed at " + app.crashed_d + " m and " + crashed_d_frm_runway + ...
                            " m over the supposed landing zone.";

                    end

                end


            elseif app.good_landing == 1
                if app.fail_theta == 1

                    max_glide = max(app.value_glide_angle)
                    app.ShowConclusion.Text = "WARNING was triggered!" + newline + ...
                        "The glide angle is too big at -" + max_glide + "°." + newline + ...
                        "The aircraft landed safely on the runway at " + app.crashed_d + " m.";

                else

                    app.ShowConclusion.Text = "No warning was triggered!" + newline + ...
                        "The aircraft landed safely on the runway at " + app.crashed_d + " m.";

                end

            end
        end

        %Export Estimated States data
        filename = 'estimated_states.xlsx';
        writetable(app.exp_T, filename, 'Sheet', 1, 'Range', 'D1')

        %Export Theoretical States data
        filename = 'theoretical_states_theo.xlsx';
        writetable(app.exp_T_theo, filename, 'Sheet', 1, 'Range', 'D1')
    end

    % Callback function
    function FinishButtonPushed(app, event)

    end

    % Button pushed function: ResetButton
    function ResetButtonPushed(app, event)
        %Reset Program for new inputs
        app.TabGroup.SelectedTab = app.InitialSettingTab;
        close all
        cla(app.UIAxes,'reset');
        cla(app.UIAxes2,'reset');
        cla(app.UIAxes3,'reset');
        cla(app.UIAxes4,'reset');
        cla(app.UIAxes5,'reset');
        cla(app.UIAxes6,'reset');
        cla(app.UIAxes7,'reset');
        cla(app.UIAxes8,'reset');
        app.flag1 = 0;
        app.flag2 = 0;
    end

    % Button pushed function: StartButton
    function StartButtonPushed(app, event)
       app.TabGroup.SelectedTab = app.InitialSettingTab;

       filepath = 'C:\Users\Jason Har\OneDrive\Documents\Year 4\FYP\FYP2\Program\';
       filename = 'Sampling_Rate_Illus.png';
       fullname = [filepath, filename];
       ImageFile = imread(fullname);
       imagesc(app.UIAxes10, ImageFile);

       close all
    end

    % Value changed function: PredefinedMicroburstDropDown
    function PredefinedMicroburstDropDownValueChanged(app, event)
        %Predefined microburst data from 3 historical microbursts
        if app.PredefinedMicroburstDropDown.Value == "Simulated Microburst by Stratten and Stengel (1993)"

            app.MaximumHorizontalWindSpeedEditField.Value = 58.4;
            app.AltitudeofMaximumHorizontalWindSpeedEditField.Value = 109.8042;
            app.DowndraftRadiusEditField.Value = 630.9360;

        elseif app.PredefinedMicroburstDropDown.Value == "Andrews A.F.B. Downburst (2000)"

            app.MaximumHorizontalWindSpeedEditField.Value = 58;
            app.AltitudeofMaximumHorizontalWindSpeedEditField.Value = 50;
            app.DowndraftRadiusEditField.Value = 1500;

        elseif app.PredefinedMicroburstDropDown.Value == "Runway 07L at Hong Kong International AIrport (2015)"

            app.MaximumHorizontalWindSpeedEditField.Value = 38;
            app.AltitudeofMaximumHorizontalWindSpeedEditField.Value = 100;
            app.DowndraftRadiusEditField.Value = 1564;

        end
    end

    % Value changed function: PredefinedAircraftStatesDropDown
    function PredefinedAircraftStatesDropDownValueChanged(app, event)
        %Predefined aircraft's initial state data for 3 locations
        if app.PredefinedAircraftStatesDropDown.Value == "Location 1"

            app.DistancefromrunwaymEditField.Value=1000;
            app.DistancefrommicroburstmEditField.Value=1500;
            app.GroundSpeedmsEditField.Value=50;
            app.AltitudemEditField.Value=100;
            app.DescentRatemsEditField.Value=0.5;

        elseif app.PredefinedAircraftStatesDropDown.Value == "Location 2"

            app.DistancefromrunwaymEditField.Value=2000;
            app.DistancefrommicroburstmEditField.Value=1500;
            app.GroundSpeedmsEditField.Value=50;
            app.AltitudemEditField.Value=100;
            app.DescentRatemsEditField.Value=0.5;

        elseif app.PredefinedAircraftStatesDropDown.Value == "Location 3"

            app.DistancefromrunwaymEditField.Value=2000;
            app.DistancefrommicroburstmEditField.Value=2000;
            app.GroundSpeedmsEditField.Value=50;
            app.AltitudemEditField.Value=100;
            app.DescentRatemsEditField.Value=0.5;

        end
    end
end

% Component initialization
methods (Access = private)

    % Create UIFigure and components
    function createComponents(app)

        % Create PredictiveWindShearEmergencyAlertSystemPrototype and hide until all components are created
        app.PredictiveWindShearEmergencyAlertSystemPrototype = uifigure('Visible', 'off');
        app.PredictiveWindShearEmergencyAlertSystemPrototype.Position = [100 100 640 480];
        app.PredictiveWindShearEmergencyAlertSystemPrototype.Name = 'Predictive Wind Shear Emergency Alert System Prototype';

        % Create TabGroup
        app.TabGroup = uitabgroup(app.PredictiveWindShearEmergencyAlertSystemPrototype);
        app.TabGroup.Position = [1 -11 732 492];

        % Create IntroductionTab
        app.IntroductionTab = uitab(app.TabGroup);
        app.IntroductionTab.Title = 'Introduction';

        % Create StartButton
        app.StartButton = uibutton(app.IntroductionTab, 'push');
        app.StartButton.ButtonPushedFcn = createCallbackFcn(app, @StartButtonPushed, true);
        app.StartButton.FontWeight = 'bold';
        app.StartButton.Position = [270 29 100 22];
        app.StartButton.Text = 'Start';

        % Create UIAxes9
        app.UIAxes9 = uiaxes(app.IntroductionTab);
        title(app.UIAxes9, 'Title')
        xlabel(app.UIAxes9, 'X')
        ylabel(app.UIAxes9, 'Y')
        app.UIAxes9.Visible = 'off';
        app.UIAxes9.Position = [108 62 424 211];

        % Create PredictiveWindShearEmergencyAlertSystemPrototypev10Label
        app.PredictiveWindShearEmergencyAlertSystemPrototypev10Label = uilabel(app.IntroductionTab);
        app.PredictiveWindShearEmergencyAlertSystemPrototypev10Label.FontWeight = 'bold';
        app.PredictiveWindShearEmergencyAlertSystemPrototypev10Label.Position = [55 295 518 163];
        app.PredictiveWindShearEmergencyAlertSystemPrototypev10Label.Text = {'Predictive Wind Shear Emergency Alert System v1.0'; ''; 'Program by: Har Chun Hong'; ''; 'Disclaimer:'; 'This program is created to serve as a demonstration for the application of the linear'; 'Kalman filter (LKF) algorithm in aircraft trajectory prediction under the influence of the'; 'microburst weather phenomenon. The coding involved in this program does not in any'; 'way represent the actual physical conditions of aircraft motion and weather but rather'; 'an approximation of the real life mechanics based on established theories in particle'; 'motion and weather modelling. Thus the program in its current state should not be used'; 'in any real life aircraft control systems due to a possibility of catastrophic damage.'};

        % Create InitialSettingTab
        app.InitialSettingTab = uitab(app.TabGroup);
        app.InitialSettingTab.Title = 'Initial Setting';

        % Create sLabel
        app.sLabel = uilabel(app.InitialSettingTab);
        app.sLabel.Position = [258 402 25 22];
        app.sLabel.Text = 's';

        % Create NextButton_3
        app.NextButton_3 = uibutton(app.InitialSettingTab, 'push');
        app.NextButton_3.ButtonPushedFcn = createCallbackFcn(app, @NextButton_3Pushed, true);
        app.NextButton_3.FontWeight = 'bold';
        app.NextButton_3.Position = [517 35 100 22];
        app.NextButton_3.Text = 'Next';

        % Create SamplingTimetEditFieldLabel
        app.SamplingTimetEditFieldLabel = uilabel(app.InitialSettingTab);
        app.SamplingTimetEditFieldLabel.HorizontalAlignment = 'right';
        app.SamplingTimetEditFieldLabel.Position = [41 402 95 22];
        app.SamplingTimetEditFieldLabel.Text = 'Sampling Time, t';

        % Create SamplingTimetEditField
        app.SamplingTimetEditField = uieditfield(app.InitialSettingTab, 'numeric');
        app.SamplingTimetEditField.Position = [151 402 100 22];
        app.SamplingTimetEditField.Value = 5;

        % Create LengthofRunwayLEditFieldLabel
        app.LengthofRunwayLEditFieldLabel = uilabel(app.InitialSettingTab);
        app.LengthofRunwayLEditFieldLabel.HorizontalAlignment = 'right';
        app.LengthofRunwayLEditFieldLabel.Position = [21 365 115 22];
        app.LengthofRunwayLEditFieldLabel.Text = 'Length of Runway, L';

        % Create LengthofRunwayLEditField
        app.LengthofRunwayLEditField = uieditfield(app.InitialSettingTab, 'numeric');
        app.LengthofRunwayLEditField.Position = [151 365 100 22];
        app.LengthofRunwayLEditField.Value = 1000;

        % Create mLabel_3
        app.mLabel_3 = uilabel(app.InitialSettingTab);
        app.mLabel_3.Position = [258 365 25 22];
        app.mLabel_3.Text = 'm';

        % Create UIAxes10
        app.UIAxes10 = uiaxes(app.InitialSettingTab);
        title(app.UIAxes10, 'Title')
        xlabel(app.UIAxes10, 'X')
        ylabel(app.UIAxes10, 'Y')
        app.UIAxes10.Visible = 'off';
        app.UIAxes10.Position = [76 72 442 269];

        % Create MicroburstSpecificationsTab
        app.MicroburstSpecificationsTab = uitab(app.TabGroup);
        app.MicroburstSpecificationsTab.Title = 'Microburst Specifications';

        % Create mLabel_2
        app.mLabel_2 = uilabel(app.MicroburstSpecificationsTab);
        app.mLabel_2.Position = [381 336 25 22];
        app.mLabel_2.Text = 'm';

        % Create UIAxes
        app.UIAxes = uiaxes(app.MicroburstSpecificationsTab);
        title(app.UIAxes, 'Graph of Y against X')
        xlabel(app.UIAxes, 'X')
        ylabel(app.UIAxes, 'Y')
        app.UIAxes.PlotBoxAspectRatio = [1.88326848249027 1 1];
        app.UIAxes.Position = [0 60 370 232];

        % Create PlotButton
        app.PlotButton = uibutton(app.MicroburstSpecificationsTab, 'push');
        app.PlotButton.ButtonPushedFcn = createCallbackFcn(app, @PlotButtonPushed, true);
        app.PlotButton.FontWeight = 'bold';
        app.PlotButton.Position = [540 346 100 22];
        app.PlotButton.Text = 'Plot';

        % Create ktsLabel
        app.ktsLabel = uilabel(app.MicroburstSpecificationsTab);
        app.ktsLabel.Position = [381 398 25 22];
        app.ktsLabel.Text = 'kts';

        % Create MaximumHorizontalWindSpeedLabel
        app.MaximumHorizontalWindSpeedLabel = uilabel(app.MicroburstSpecificationsTab);
        app.MaximumHorizontalWindSpeedLabel.HorizontalAlignment = 'right';
        app.MaximumHorizontalWindSpeedLabel.Position = [73 398 183 22];
        app.MaximumHorizontalWindSpeedLabel.Text = 'Maximum Horizontal Wind Speed';

        % Create MaximumHorizontalWindSpeedEditField
        app.MaximumHorizontalWindSpeedEditField = uieditfield(app.MicroburstSpecificationsTab, 'numeric');
        app.MaximumHorizontalWindSpeedEditField.Position = [271 398 100 22];
        app.MaximumHorizontalWindSpeedEditField.Value = 58.4;

        % Create NextButton
        app.NextButton = uibutton(app.MicroburstSpecificationsTab, 'push');
        app.NextButton.ButtonPushedFcn = createCallbackFcn(app, @NextButtonPushed, true);
        app.NextButton.FontWeight = 'bold';
        app.NextButton.Enable = 'off';
        app.NextButton.Position = [540 25 100 22];
        app.NextButton.Text = 'Next';

        % Create DowndraftRadiusEditFieldLabel
        app.DowndraftRadiusEditFieldLabel = uilabel(app.MicroburstSpecificationsTab);
        app.DowndraftRadiusEditFieldLabel.HorizontalAlignment = 'right';
        app.DowndraftRadiusEditFieldLabel.Position = [155 336 101 22];
        app.DowndraftRadiusEditFieldLabel.Text = 'Downdraft Radius';

        % Create DowndraftRadiusEditField
        app.DowndraftRadiusEditField = uieditfield(app.MicroburstSpecificationsTab, 'numeric');
        app.DowndraftRadiusEditField.Position = [271 336 100 22];
        app.DowndraftRadiusEditField.Value = 630.936;

        % Create DisplayWindDirectionSwitchLabel
        app.DisplayWindDirectionSwitchLabel = uilabel(app.MicroburstSpecificationsTab);
        app.DisplayWindDirectionSwitchLabel.HorizontalAlignment = 'center';
        app.DisplayWindDirectionSwitchLabel.Position = [527 379 126 22];
        app.DisplayWindDirectionSwitchLabel.Text = 'Display Wind Direction';

        % Create DisplayWindDirectionSwitch
        app.DisplayWindDirectionSwitch = uiswitch(app.MicroburstSpecificationsTab, 'slider');
        app.DisplayWindDirectionSwitch.Items = {'Horizontal', 'Vertical'};
        app.DisplayWindDirectionSwitch.Enable = 'off';
        app.DisplayWindDirectionSwitch.Position = [575 402 45 20];
        app.DisplayWindDirectionSwitch.Value = 'Horizontal';

        % Create SimulatenoisyinputsButton
        app.SimulatenoisyinputsButton = uibutton(app.MicroburstSpecificationsTab, 'push');
        app.SimulatenoisyinputsButton.ButtonPushedFcn = createCallbackFcn(app, @SimulatenoisyinputsButtonPushed, true);
        app.SimulatenoisyinputsButton.FontWeight = 'bold';
        app.SimulatenoisyinputsButton.Enable = 'off';
        app.SimulatenoisyinputsButton.Position = [520 315 140 22];
        app.SimulatenoisyinputsButton.Text = 'Simulate noisy inputs';

        % Create UIAxes6
        app.UIAxes6 = uiaxes(app.MicroburstSpecificationsTab);
        title(app.UIAxes6, 'Graph of Y against X')
        xlabel(app.UIAxes6, 'X')
        ylabel(app.UIAxes6, 'Y')
        app.UIAxes6.PlotBoxAspectRatio = [1.7737556561086 1 1];
        app.UIAxes6.Position = [369 60 361 232];

        % Create AltitudeofMaximumHorizontalWindSpeedEditFieldLabel
        app.AltitudeofMaximumHorizontalWindSpeedEditFieldLabel = uilabel(app.MicroburstSpecificationsTab);
        app.AltitudeofMaximumHorizontalWindSpeedEditFieldLabel.HorizontalAlignment = 'right';
        app.AltitudeofMaximumHorizontalWindSpeedEditFieldLabel.Position = [14 366 242 22];
        app.AltitudeofMaximumHorizontalWindSpeedEditFieldLabel.Text = 'Altitude of Maximum Horizontal Wind Speed';

        % Create AltitudeofMaximumHorizontalWindSpeedEditField
        app.AltitudeofMaximumHorizontalWindSpeedEditField = uieditfield(app.MicroburstSpecificationsTab, 'numeric');
        app.AltitudeofMaximumHorizontalWindSpeedEditField.Position = [271 367 100 22];
        app.AltitudeofMaximumHorizontalWindSpeedEditField.Value = 109.8042;

        % Create mLabel_4
        app.mLabel_4 = uilabel(app.MicroburstSpecificationsTab);
        app.mLabel_4.Position = [381 367 25 22];
        app.mLabel_4.Text = 'm';

        % Create PredefinedMicroburstDropDownLabel
        app.PredefinedMicroburstDropDownLabel = uilabel(app.MicroburstSpecificationsTab);
        app.PredefinedMicroburstDropDownLabel.HorizontalAlignment = 'right';
        app.PredefinedMicroburstDropDownLabel.Position = [130 432 127 22];
        app.PredefinedMicroburstDropDownLabel.Text = 'Pre-defined Microburst';

        % Create PredefinedMicroburstDropDown
        app.PredefinedMicroburstDropDown = uidropdown(app.MicroburstSpecificationsTab);
        app.PredefinedMicroburstDropDown.Items = {'Simulated Microburst by Stratten and Stengel (1993)', 'Andrews A.F.B. Downburst (2000)', 'Runway 07L at Hong Kong International AIrport (2015)'};
        app.PredefinedMicroburstDropDown.ValueChangedFcn = createCallbackFcn(app, @PredefinedMicroburstDropDownValueChanged, true);
        app.PredefinedMicroburstDropDown.Position = [279 432 323 22];
        app.PredefinedMicroburstDropDown.Value = 'Simulated Microburst by Stratten and Stengel (1993)';

        % Create InitialAircraftStateTab
        app.InitialAircraftStateTab = uitab(app.TabGroup);
        app.InitialAircraftStateTab.Title = 'Initial Aircraft State';

        % Create NextButton_2
        app.NextButton_2 = uibutton(app.InitialAircraftStateTab, 'push');
        app.NextButton_2.ButtonPushedFcn = createCallbackFcn(app, @NextButton_2Pushed, true);
        app.NextButton_2.FontWeight = 'bold';
        app.NextButton_2.Position = [514 27 100 22];
        app.NextButton_2.Text = 'Next';

        % Create PredefinedAircraftStatesDropDownLabel
        app.PredefinedAircraftStatesDropDownLabel = uilabel(app.InitialAircraftStateTab);
        app.PredefinedAircraftStatesDropDownLabel.HorizontalAlignment = 'right';
        app.PredefinedAircraftStatesDropDownLabel.Position = [26 420 150 22];
        app.PredefinedAircraftStatesDropDownLabel.Text = 'Pre-defined Aircraft States:';

        % Create PredefinedAircraftStatesDropDown
        app.PredefinedAircraftStatesDropDown = uidropdown(app.InitialAircraftStateTab);
        app.PredefinedAircraftStatesDropDown.Items = {'Location 1', 'Location 2', 'Location 3'};
        app.PredefinedAircraftStatesDropDown.ValueChangedFcn = createCallbackFcn(app, @PredefinedAircraftStatesDropDownValueChanged, true);
        app.PredefinedAircraftStatesDropDown.Position = [191 420 100 22];
        app.PredefinedAircraftStatesDropDown.Value = 'Location 1';

        % Create DistancefromrunwaymEditFieldLabel
        app.DistancefromrunwaymEditFieldLabel = uilabel(app.InitialAircraftStateTab);
        app.DistancefromrunwaymEditFieldLabel.HorizontalAlignment = 'right';
        app.DistancefromrunwaymEditFieldLabel.Position = [42 370 143 22];
        app.DistancefromrunwaymEditFieldLabel.Text = 'Distance from runway (m)';

        % Create DistancefromrunwaymEditField
        app.DistancefromrunwaymEditField = uieditfield(app.InitialAircraftStateTab, 'numeric');
        app.DistancefromrunwaymEditField.Position = [200 370 100 22];
        app.DistancefromrunwaymEditField.Value = 1000;

        % Create DistancefrommicroburstmEditFieldLabel
        app.DistancefrommicroburstmEditFieldLabel = uilabel(app.InitialAircraftStateTab);
        app.DistancefrommicroburstmEditFieldLabel.HorizontalAlignment = 'right';
        app.DistancefrommicroburstmEditFieldLabel.Position = [25 327 160 22];
        app.DistancefrommicroburstmEditFieldLabel.Text = 'Distance from microburst (m)';

        % Create DistancefrommicroburstmEditField
        app.DistancefrommicroburstmEditField = uieditfield(app.InitialAircraftStateTab, 'numeric');
        app.DistancefrommicroburstmEditField.Position = [200 327 100 22];
        app.DistancefrommicroburstmEditField.Value = 1500;

        % Create AltitudemEditFieldLabel
        app.AltitudemEditFieldLabel = uilabel(app.InitialAircraftStateTab);
        app.AltitudemEditFieldLabel.HorizontalAlignment = 'right';
        app.AltitudemEditFieldLabel.Position = [118 284 67 22];
        app.AltitudemEditFieldLabel.Text = 'Altitude (m)';

        % Create AltitudemEditField
        app.AltitudemEditField = uieditfield(app.InitialAircraftStateTab, 'numeric');
        app.AltitudemEditField.Position = [200 284 100 22];
        app.AltitudemEditField.Value = 100;

        % Create GroundSpeedmsEditFieldLabel
        app.GroundSpeedmsEditFieldLabel = uilabel(app.InitialAircraftStateTab);
        app.GroundSpeedmsEditFieldLabel.HorizontalAlignment = 'right';
        app.GroundSpeedmsEditFieldLabel.Position = [326 327 114 22];
        app.GroundSpeedmsEditFieldLabel.Text = 'Ground Speed (m/s)';

        % Create GroundSpeedmsEditField
        app.GroundSpeedmsEditField = uieditfield(app.InitialAircraftStateTab, 'numeric');
        app.GroundSpeedmsEditField.Position = [455 327 100 22];
        app.GroundSpeedmsEditField.Value = 50;

        % Create DescentRatemsEditFieldLabel
        app.DescentRatemsEditFieldLabel = uilabel(app.InitialAircraftStateTab);
        app.DescentRatemsEditFieldLabel.HorizontalAlignment = 'right';
        app.DescentRatemsEditFieldLabel.Position = [331 284 109 22];
        app.DescentRatemsEditFieldLabel.Text = 'Descent Rate (m/s)';

        % Create DescentRatemsEditField
        app.DescentRatemsEditField = uieditfield(app.InitialAircraftStateTab, 'numeric');
        app.DescentRatemsEditField.Position = [455 284 100 22];
        app.DescentRatemsEditField.Value = 0.5;

        % Create StateEstimatorTab
        app.StateEstimatorTab = uitab(app.TabGroup);
        app.StateEstimatorTab.Title = 'State Estimator';

        % Create ComputeButton
        app.ComputeButton = uibutton(app.StateEstimatorTab, 'push');
        app.ComputeButton.ButtonPushedFcn = createCallbackFcn(app, @ComputeButtonPushed, true);
        app.ComputeButton.FontWeight = 'bold';
        app.ComputeButton.Position = [406 17 100 22];
        app.ComputeButton.Text = 'Compute';

        % Create UIAxes2
        app.UIAxes2 = uiaxes(app.StateEstimatorTab);
        title(app.UIAxes2, 'Graph of Y against X')
        xlabel(app.UIAxes2, 'X')
        ylabel(app.UIAxes2, 'Y')
        app.UIAxes2.PlotBoxAspectRatio = [1.94444444444444 1 1];
        app.UIAxes2.Position = [20 252 300 185];

        % Create UIAxes3
        app.UIAxes3 = uiaxes(app.StateEstimatorTab);
        title(app.UIAxes3, 'Graph of Y against X')
        xlabel(app.UIAxes3, 'X')
        ylabel(app.UIAxes3, 'Y')
        app.UIAxes3.PlotBoxAspectRatio = [1.94444444444444 1 1];
        app.UIAxes3.Position = [20 56 300 185];

        % Create UIAxes4
        app.UIAxes4 = uiaxes(app.StateEstimatorTab);
        title(app.UIAxes4, 'Graph of Y against X')
        xlabel(app.UIAxes4, 'X')
        ylabel(app.UIAxes4, 'Y')
        app.UIAxes4.PlotBoxAspectRatio = [1.94444444444444 1 1];
        app.UIAxes4.Position = [319 252 300 185];

        % Create UIAxes5
        app.UIAxes5 = uiaxes(app.StateEstimatorTab);
        title(app.UIAxes5, 'Graph of Y against X')
        xlabel(app.UIAxes5, 'X')
        ylabel(app.UIAxes5, 'Y')
        app.UIAxes5.PlotBoxAspectRatio = [1.94444444444444 1 1];
        app.UIAxes5.Position = [319 56 300 185];

        % Create NextButton_4
        app.NextButton_4 = uibutton(app.StateEstimatorTab, 'push');
        app.NextButton_4.ButtonPushedFcn = createCallbackFcn(app, @NextButton_4Pushed, true);
        app.NextButton_4.FontWeight = 'bold';
        app.NextButton_4.Position = [531 17 100 22];
        app.NextButton_4.Text = 'Next';

        % Create GlideAngleandSeverityFactorTab
        app.GlideAngleandSeverityFactorTab = uitab(app.TabGroup);
        app.GlideAngleandSeverityFactorTab.Title = 'Glide Angle and Severity Factor';

        % Create UIAxes7
        app.UIAxes7 = uiaxes(app.GlideAngleandSeverityFactorTab);
        title(app.UIAxes7, 'Graph of Y against X')
        xlabel(app.UIAxes7, 'X')
        ylabel(app.UIAxes7, 'Y')
        app.UIAxes7.Position = [2 263 729 204];

        % Create UIAxes8
        app.UIAxes8 = uiaxes(app.GlideAngleandSeverityFactorTab);
        title(app.UIAxes8, 'Graph of Y against X')
        xlabel(app.UIAxes8, 'X')
        ylabel(app.UIAxes8, 'Y')
        app.UIAxes8.Position = [0 49 729 215];

        % Create NextButton_5
        app.NextButton_5 = uibutton(app.GlideAngleandSeverityFactorTab, 'push');
        app.NextButton_5.ButtonPushedFcn = createCallbackFcn(app, @NextButton_5Pushed, true);
        app.NextButton_5.FontWeight = 'bold';
        app.NextButton_5.Position = [331 20 100 22];
        app.NextButton_5.Text = 'Next';

        % Create ConclusionTab
        app.ConclusionTab = uitab(app.TabGroup);
        app.ConclusionTab.Title = 'Conclusion';

        % Create ResetButton
        app.ResetButton = uibutton(app.ConclusionTab, 'push');
        app.ResetButton.ButtonPushedFcn = createCallbackFcn(app, @ResetButtonPushed, true);
        app.ResetButton.FontWeight = 'bold';
        app.ResetButton.Position = [478 46 100 22];
        app.ResetButton.Text = 'Reset';

        % Create ShowConclusion
        app.ShowConclusion = uilabel(app.ConclusionTab);
        app.ShowConclusion.Position = [78 128 562 276];
        app.ShowConclusion.Text = '';

        % Show the figure after all components are created
        app.PredictiveWindShearEmergencyAlertSystemPrototype.Visible = 'on';
    end
end

% App creation and deletion
methods (Access = public)

    % Construct app
    function app = Program

        % Create UIFigure and components
        createComponents(app)

        % Register the app with App Designer
        registerApp(app, app.PredictiveWindShearEmergencyAlertSystemPrototype)

        % Execute the startup function
        runStartupFcn(app, @startupFcn)

        if nargout == 0
            clear app
        end
    end

    % Code that executes before app deletion
    function delete(app)

        % Delete UIFigure when app is deleted
        delete(app.PredictiveWindShearEmergencyAlertSystemPrototype)
    end
end
